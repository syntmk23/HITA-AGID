<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/enchere.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$enchere = new Enchere($db);
 
// query products
$stmt = $enchere->voir_tous_message();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $encheres_arr=array();
    $encheres_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $enchere_item=array(
            "nom_utilisateur" => $nom_utilisateur,
            "id_chat" => $id_chat,
            "description_chat" => $description_chat,
            "phrase" => $phrase,
            "date_envoie" => $date_envoie
        );
 
       	if($nom_utilisateur!=null){
			array_push($encheres_arr["records"], $enchere_item);
		}
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data in json format
    echo json_encode($encheres_arr);
}
 
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No message found.")
    );
}