<?php
class Utilisateur{
 
    // database connection and table name
    private $conn;
    private $table_name = "utilisateur";
 
    // object properties
    public $id_utilisateur;
    public $nom_utilisateur;
    public $pseudo;
    public $mot_de_passe;
 
    public function __construct($db){
        $this->conn = $db;
    }
 
    // used by select drop-down list
    public function voir_tous(){
        //select all data
        $query = "SELECT id_utilisateur,nom_utilisateur,pseudo as surnom, mot_de_passe 
		FROM " . $this->table_name . " ORDER BY id_utilisateur";
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
			//creer un utilisateur
	function creer(){
 
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					nom_utilisateur=:nom_utilisateur, 
					pseudo=:pseudo, mot_de_passe=:mot_de_passe";
 
		// prepare query
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->nom_utilisateur=htmlspecialchars(strip_tags($this->nom_utilisateur));
		$this->pseudo=htmlspecialchars(strip_tags($this->pseudo));
		$this->mot_de_passe=htmlspecialchars(strip_tags($this->mot_de_passe));
 
		// bind values
		$stmt->bindParam(":nom_utilisateur", $this->nom_utilisateur);
		$stmt->bindParam(":pseudo", $this->pseudo);
		$stmt->bindParam(":mot_de_passe", $this->mot_de_passe);
 
		// execute query
		if($stmt->execute()){
			return true;
		}
 
		return false;    
	}
			//fonction pour mise a jour
	function mise_a_jour(){
 
		// update query
		$query = "UPDATE
					" . $this->table_name . "
				SET
					nom_utilisateur = :nom_utilisateur,
					pseudo = :pseudo,
					mot_de_passe = :mot_de_passe
				WHERE
					id_utilisateur = :id_utilisateur";
 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->nom_utilisateur=htmlspecialchars(strip_tags($this->nom_utilisateur));
		$this->pseudo=htmlspecialchars(strip_tags($this->pseudo));
		$this->mot_de_passe=htmlspecialchars(strip_tags($this->mot_de_passe));
		$this->id_utilisateur=htmlspecialchars(strip_tags($this->id_utilisateur));
 
		// bind new values
		$stmt->bindParam(':mot_de_passe', $this->mot_de_passe);
		$stmt->bindParam(':nom_utilisateur', $this->nom_utilisateur);
		$stmt->bindParam(':pseudo', $this->pseudo);
		$stmt->bindParam(':id_utilisateur', $this->id_utilisateur);
 
		// execute the query
		if($stmt->execute()){
			return true;
		}
 
		return false;
	}
	// delete un utilisateur
	function supprimer(){
 
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE id_utilisateur = ?";
 
		// prepare query
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->id_utilisateur=htmlspecialchars(strip_tags($this->id_utilisateur));
 
		// bind id of record to delete
		$stmt->bindParam(1, $this->id_utilisateur);
 
		// execute query
		if($stmt->execute()){
			return true;
		}
 
		return false;     
	}
		// used when filling up the update chat form
	function voir_un_seul(){
 
		// query to read single record
		$query = "SELECT id_utilisateur, nom_utilisateur,pseudo as surnom, mot_de_passe 
		FROM " . $this->table_name . " WHERE id_utilisateur = ?";
 
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
 
		// bind id of product to be updated
		$stmt->bindParam(1, $this->id_utilisateur);

		//$stmt->bindParam(2, $this->num_article);
 
		// execute query
		$stmt->execute();
 
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
 
		// set values to object properties
		$this->id_utilisateur = $row['id_utilisateur'];
		$this->nom_utilisateur = $row['nom_utilisateur'];
		$this->surnom = $row['surnom'];
		$this->mot_de_passe = $row['mot_de_passe'];
	}
}
?>