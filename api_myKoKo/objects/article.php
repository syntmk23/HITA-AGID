<?php
class Article{
 
    // database connection and table name
    private $conn;
    private $table_name = "article";
 
    // object properties
    public $id_article;
    public $nom_article;
	public $vendu;
    public $date_de_publication;
    public $date_vendu;
    public $commentaire_acheteur;
 
    public function __construct($db){
        $this->conn = $db;
    }
 
    // used by select drop-down list
    public function voir_tous_article(){
        //select all data
        $query = "SELECT
					nom_article, pseudo as nom_utilisateur, vend, date_de_publication, date_vendu as date_de_vente , commentaire_acheteur
				FROM
					" . $this->table_name . " a
					RIGHT JOIN
						action ac
							ON a.id_article = ac.numero_article
					RIGHT JOIN
						utilisateur ut
							ON ut.id_utilisateur = ac.numero_utilisateur";
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }

	// used by select drop-down list
    public function voir_article_vendeur(){
        //select all data
        $query = "SELECT
					nom_article, pseudo as nom_utilisateur, date_vendu as date_de_vente
				FROM
					" . $this->table_name . " a
					RIGHT JOIN
						action ac
							ON a.id_article = ac.numero_article
					RIGHT JOIN
						utilisateur ut
							ON ut.id_utilisateur = ac.numero_utilisateur
							WHERE vend = 1";
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }

	// used by select drop-down list
    public function voir_article_acheteur(){
        //select all data
        $query = "SELECT
					nom_article, pseudo as nom_utilisateur, date_vendu as date_de_vente, commentaire_acheteur
				FROM
					" . $this->table_name . " a
					RIGHT JOIN
						action ac
							ON a.id_article = ac.numero_article
					RIGHT JOIN
						utilisateur ut
							ON ut.id_utilisateur = ac.numero_utilisateur
							WHERE vend = 0";
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
	// used by select drop-down list
    public function voir_article_par_nom(){
        //select all data
        $query = "SELECT
					nom_article, pseudo as nom_utilisateur, date_vendu, commentaire_acheteur
				FROM
					" . $this->table_name . " a
					RIGHT JOIN
						action ac
							ON a.id_article = ac.numero_article
					RIGHT JOIN
						utilisateur ut
							ON ut.id_utilisateur = ac.numero_utilisateur
							WHERE vend = 0";
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
	// create product
	function creer_article(){
 
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					nom_article=:nom_article, vendu=:vendu, date_de_publication=:date_de_publication";
 
		// prepare query
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->nom_article=htmlspecialchars(strip_tags($this->nom_article));
		$this->vendu=htmlspecialchars(strip_tags($this->vendu));
		$this->date_de_publication=htmlspecialchars(strip_tags($this->date_de_publication));
 
		// bind values
		$stmt->bindParam(":nom_article", $this->nom_article);
		$stmt->bindParam(":vendu", $this->vendu);
		$stmt->bindParam(":date_de_publication", $this->date_de_publication);
 
		// execute query
		if($stmt->execute()){
			return true;
		}
 
		return false;    
	}
	// used when filling up the update product form
// used when filling up the update product form
	function voir_article_par_son_nom(){
 
		// query to read single record
		$query = "SELECT id_article, nom_article, vendu, pseudo as nom_utilisateur, vend as vendeur, date_de_publication, date_vendu , commentaire_acheteur
		FROM " . $this->table_name . " RIGHT JOIN action ON article.id_article = action.numero_article
		RIGHT JOIN utilisateur ON utilisateur.id_utilisateur = action.numero_utilisateur WHERE nom_article = ? ";
 
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		// sanitize
		$this->nom_article=htmlspecialchars(strip_tags($this->nom_article));
		// bind new values
		$stmt->bindParam(1, $this->nom_article);
		$stmt->execute();
 
		return $stmt;
	}
	//voir par son id
	function voir_article_par_son_id(){
 
		// query to read single record
		$query = "SELECT id_article, nom_article, vendu, pseudo as nom_utilisateur, vend as vendeur, date_de_publication, date_vendu , commentaire_acheteur
		FROM " . $this->table_name . " RIGHT JOIN action ON article.id_article = action.numero_article
		RIGHT JOIN utilisateur ON utilisateur.id_utilisateur = action.numero_utilisateur WHERE id_article = ? ";
 
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		// sanitize
		$this->id_article=htmlspecialchars(strip_tags($this->id_article));
		// bind new values
		$stmt->bindParam(1, $this->id_article);
		$stmt->execute();
 
		return $stmt;
	}
	// update the product
	function mise_a_jour(){
 
		// update query
		$query = "UPDATE
					" . $this->table_name . "
				SET
					nom_article = :nom_article,
					vendu = :vendu,
					date_de_publication = :date_de_publication,
					date_vendu = :date_vendu,
					commentaire_acheteur = :commentaire_acheteur
				WHERE
					id_article = :id_article";
 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->nom_article=htmlspecialchars(strip_tags($this->nom_article));
		$this->vendu=htmlspecialchars(strip_tags($this->vendu));
		$this->date_de_publication=htmlspecialchars(strip_tags($this->date_de_publication));
		$this->date_vendu=htmlspecialchars(strip_tags($this->date_vendu));
		$this->commentaire_acheteur=htmlspecialchars(strip_tags($this->commentaire_acheteur));
		$this->id_article=htmlspecialchars(strip_tags($this->id_article));
 
		// bind new values
		$stmt->bindParam(':id_article', $this->id_article);
		$stmt->bindParam(':nom_article', $this->nom_article);
		$stmt->bindParam(':vendu', $this->vendu);
		$stmt->bindParam(':date_de_publication', $this->date_de_publication);
		$stmt->bindParam(':date_vendu', $this->date_vendu);
		$stmt->bindParam(':commentaire_acheteur', $this->commentaire_acheteur);
 
		// execute the query
		if($stmt->execute()){
			return true;
		}
 
		return false;
	}
	// delete the product
	function supprimer(){
 
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE id_article = ?";
 
		// prepare query
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->id_article=htmlspecialchars(strip_tags($this->id_article));
 
		// bind id of record to delete
		$stmt->bindParam(1, $this->id_article);
 
		// execute query
		if($stmt->execute()){
			return true;
		}
 
		return false;     
	}
}
?>