<?php
class Chat{
 
    // database connection and table name
    private $conn;
    private $table_name = "chat";
 
    // object properties
    public $id_chat;
    public $description_chat;
    public $article_concerne;
    public $num_utilisateur;
 
    public function __construct($db){
        $this->conn = $db;
    }
 
    // used by select drop-down list
		// used by select drop-down list
	public function voir_tous_chat(){
 
		//select all data
		$query = "SELECT id_chat, description_chat, id_article, nom_article, 
		id_utilisateur, pseudo as admin, vendu, date_de_publication, date_vendu as date_de_vente, commentaire_acheteur 
		FROM " . $this->table_name . "
        RIGHT JOIN article ON chat.article_concerne = article.id_article
        RIGHT JOIN utilisateur ON utilisateur.id_utilisateur = chat.num_utilisateur WHERE id_chat is not null";
 
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
 
		return $stmt;
	}
		//creer un chat
	function creer(){
 
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					description_chat=:description_chat, article_concerne=:article_concerne, 
					num_utilisateur=:num_utilisateur";
 
		// prepare query
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->description_chat=htmlspecialchars(strip_tags($this->description_chat));
		$this->article_concerne=htmlspecialchars(strip_tags($this->article_concerne));
		$this->num_utilisateur=htmlspecialchars(strip_tags($this->num_utilisateur));
 
		// bind values
		$stmt->bindParam(":description_chat", $this->description_chat);
		$stmt->bindParam(":article_concerne", $this->article_concerne);
		$stmt->bindParam(":num_utilisateur", $this->num_utilisateur);
 
		// execute query
		if($stmt->execute()){
			return true;
		}
 
		return false;    
	}
		//fonction pour mise a jour
	function mise_a_jour(){
 
		// update query
		$query = "UPDATE
					" . $this->table_name . "
				SET
					description_chat = :description_chat,
					article_concerne = :article_concerne,
					num_utilisateur = :num_utilisateur
				WHERE
					id_chat = :id_chat";
 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->description_chat=htmlspecialchars(strip_tags($this->description_chat));
		$this->article_concerne=htmlspecialchars(strip_tags($this->article_concerne));
		$this->num_utilisateur=htmlspecialchars(strip_tags($this->num_utilisateur));
		$this->id_chat=htmlspecialchars(strip_tags($this->id_chat));
 
		// bind new values
		$stmt->bindParam(':num_utilisateur', $this->num_utilisateur);
		$stmt->bindParam(':description_chat', $this->description_chat);
		$stmt->bindParam(':article_concerne', $this->article_concerne);
		$stmt->bindParam(':id_chat', $this->id_chat);
 
		// execute the query
		if($stmt->execute()){
			return true;
		}
 
		return false;
	}
		// delete the product
	function supprimer(){
 
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE id_chat = ?";
 
		// prepare query
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->id_chat=htmlspecialchars(strip_tags($this->id_chat));
 
		// bind id of record to delete
		$stmt->bindParam(1, $this->id_chat);
 
		// execute query
		if($stmt->execute()){
			return true;
		}
 
		return false;     
	}
		//voir par id_utilisateur tous
	function voir_chat_par_utilisateur(){
 
		//select all data
		$query = "SELECT id_chat, description_chat, id_article, nom_article, 
		id_utilisateur, pseudo as admin, vendu, date_de_publication, date_vendu as date_de_vente, commentaire_acheteur 
		FROM " . $this->table_name . "
        RIGHT JOIN article ON chat.article_concerne = article.id_article
        RIGHT JOIN utilisateur ON utilisateur.id_utilisateur = chat.num_utilisateur WHERE id_chat is not null 
		AND num_utilisateur = ?";
 
		$stmt = $this->conn->prepare( $query );
		// sanitize
		$this->num_utilisateur=htmlspecialchars(strip_tags($this->num_utilisateur));
		// bind new values
		$stmt->bindParam(1, $this->num_utilisateur);
		$stmt->execute();
 
		return $stmt;
	}
			//voir par id_article tous
	function voir_chat_par_article(){
 
		//select all data
		$query = "SELECT id_chat, description_chat, id_article, nom_article, 
		id_utilisateur, pseudo as admin, vendu, date_de_publication, date_vendu as date_de_vente, commentaire_acheteur 
		FROM " . $this->table_name . "
        RIGHT JOIN article ON chat.article_concerne = article.id_article
        RIGHT JOIN utilisateur ON utilisateur.id_utilisateur = chat.num_utilisateur WHERE id_chat is not null 
		AND article_concerne = ?";
 
		$stmt = $this->conn->prepare( $query );
		// sanitize
		$this->article_concerne=htmlspecialchars(strip_tags($this->article_concerne));
		// bind new values
		$stmt->bindParam(1, $this->article_concerne);
		$stmt->execute();
 
		return $stmt;
	}
	// used when filling up the update chat form
	function voir_un_seul(){
 
		// query to read single record
		$query = "SELECT id_chat, description_chat, id_article, nom_article, 
		id_utilisateur, pseudo as admin, vendu, date_de_publication, date_vendu as date_de_vente, commentaire_acheteur 
		FROM " . $this->table_name . "
        RIGHT JOIN article ON chat.article_concerne = article.id_article
        RIGHT JOIN utilisateur ON utilisateur.id_utilisateur = chat.num_utilisateur WHERE id_chat is not null 
		AND num_utilisateur = ?";
 
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
 
		// bind id of product to be updated
		$stmt->bindParam(1, $this->num_utilisateur);

		//$stmt->bindParam(2, $this->num_article);
 
		// execute query
		$stmt->execute();
 
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
 
		// set values to object properties
		$this->id_chat = $row['id_chat'];
		$this->description_chat = $row['description_chat'];
		$this->id_article = $row['id_article'];
		$this->nom_article = $row['nom_article'];
		$this->id_article = $row['id_article'];
		$this->id_utilisateur = $row['id_utilisateur'];
		$this->admin = $row['admin'];
		$this->vendu = $row['vendu'];
		$this->date_de_publication = $row['date_de_publication'];
		$this->date_de_vente = $row['date_de_vente'];
		$this->commentaire_acheteur = $row['commentaire_acheteur'];
	}
}
?>