<?php
class Enchere{
 
    // database connection and table name
    private $conn;
    private $table_name = "enchere";
 
    // object properties
    public $id_enchere;
    public $id_parleur;
    public $phrase;
    public $id_chat;
    public $date;
 
    public function __construct($db){
        $this->conn = $db;
    }

	// used by select drop-down list
	public function voir_tous_message(){
 
		//select all data
		$query = "SELECT pseudo as nom_utilisateur,numero_chat, description_chat, phrase , date_envoie FROM utilisateur
        RIGHT JOIN enchere ON utilisateur.id_utilisateur = enchere.id_parleur
        RIGHT JOIN chat ON chat.numero_chat = enchere.numero_chat ORDER BY date_envoie ASC";
 
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
 
		return $stmt;
	}
	//creer un message
	function creer(){
 
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					id_parleur=:id_parleur, phrase=:phrase, numero_chat=:numero_chat, date_envoie=:date_envoie";
 
		// prepare query
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->id_parleur=htmlspecialchars(strip_tags($this->id_parleur));
		$this->phrase=htmlspecialchars(strip_tags($this->phrase));
		$this->numero_chat=htmlspecialchars(strip_tags($this->numero_chat));
		$this->date_envoie=htmlspecialchars(strip_tags($this->date_envoie));
 
		// bind values
		$stmt->bindParam(":id_parleur", $this->id_parleur);
		$stmt->bindParam(":phrase", $this->phrase);
		$stmt->bindParam(":numero_chat", $this->numero_chat);
		$stmt->bindParam(":date_envoie", $this->date_envoie);
 
		// execute query
		if($stmt->execute()){
			return true;
		}
 
		return false;    
	}
	//fonction pour mise a jour
		function mise_a_jour(){
 
		// update query
		$query = "UPDATE
					" . $this->table_name . "
				SET
					id_parleur = :id_parleur,
					phrase = :phrase,
					numero_chat = :numero_chat,
					date_envoie = :date_envoie
				WHERE
					id_enchere = :id_enchere";
 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->id_parleur=htmlspecialchars(strip_tags($this->id_parleur));
		$this->phrase=htmlspecialchars(strip_tags($this->phrase));
		$this->numero_chat=htmlspecialchars(strip_tags($this->numero_chat));
		$this->date_envoie=htmlspecialchars(strip_tags($this->date_envoie));
		$this->id_enchere=htmlspecialchars(strip_tags($this->id_enchere));
 
		// bind new values
		$stmt->bindParam(':id_enchere', $this->id_enchere);
		$stmt->bindParam(':id_parleur', $this->id_parleur);
		$stmt->bindParam(':phrase', $this->phrase);
		$stmt->bindParam(':numero_chat', $this->numero_chat);
		$stmt->bindParam(':date_envoie', $this->date_envoie);
 
		// execute the query
		if($stmt->execute()){
			return true;
		}
 
		return false;
	}
	// delete the product
	function supprimer(){
 
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE id_enchere = ?";
 
		// prepare query
		$stmt = $this->conn->prepare($query);
 
		// sanitize
		$this->id_enchere=htmlspecialchars(strip_tags($this->id_enchere));
 
		// bind id of record to delete
		$stmt->bindParam(1, $this->id_enchere);
 
		// execute query
		if($stmt->execute()){
			return true;
		}
 
		return false;     
	}

	//voir par id_chat
	function voir_message_chat(){
 
		//select all data
		$query = "SELECT  id_enchere, nom_utilisateur,numero_chat, description_chat, phrase , date_envoie FROM utilisateur
        RIGHT JOIN enchere ON utilisateur.id_utilisateur = enchere.id_parleur
        RIGHT JOIN chat ON chat.id_chat = enchere.numero_chat WHERE id_chat = :id_chat ORDER BY date_envoie ASC";
 
		$stmt = $this->conn->prepare( $query );
		// sanitize
		$this->id_chat=htmlspecialchars(strip_tags($this->id_chat));
		// bind new values
		$stmt->bindParam(':id_chat', $this->id_chat);
		$stmt->execute();
 
		return $stmt;
	}
	//voir par id_utilisateur
	function voir_message_utilisateur(){
 
		//select all data
		$query = "SELECT  id_enchere, nom_utilisateur,numero_chat, description_chat, phrase , date_envoie FROM utilisateur
        RIGHT JOIN enchere ON utilisateur.id_utilisateur = enchere.id_parleur
        RIGHT JOIN chat ON chat.id_chat = enchere.numero_chat WHERE id_parleur = ? ORDER BY date_envoie ASC";
 
		$stmt = $this->conn->prepare( $query );
		// sanitize
		$this->id_parleur=htmlspecialchars(strip_tags($this->id_parleur));
		// bind new values
		$stmt->bindParam(1, $this->id_parleur);
		$stmt->execute();
 
		return $stmt;
	}
}
?>