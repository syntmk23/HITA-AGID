<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/article.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$article = new Article($db);
 
// get id of product to be edited
$data = json_decode(file_get_contents("php://input"));
 
// set ID property of product to be edited
$article->id_article = $data->id_article;
 
// set product property values
$article->nom_article = $data->nom_article;
$article->vendu = $data->vendu;
$article->date_de_publication = $data->date_de_publication;
$article->date_vendu = $data->date_vendu;
$article->commentaire_acheteur = $data->commentaire_acheteur;
 
// update the product
if($article->mise_a_jour()){
 
    // set response code - 200 ok
    http_response_code(200);
 
    // tell the user
    echo json_encode(array("message" => "article was updated."));
}
 
// if unable to update the product, tell the user
else{
 
    // set response code - 503 service unavailable
    http_response_code(503);
 
    // tell the user
    echo json_encode(array("message" => "Unable to update article."));
}
?>