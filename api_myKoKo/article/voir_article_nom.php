<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/article.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$article = new Article($db);
 // set ID property of record to read
$article->nom_article = isset($_GET['nom_article']) ? $_GET['nom_article'] : die();
// query products
$stmt = $article->voir_article_par_son_nom();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $articles_arr=array();
    $articles_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $article_item=array(
            "id_article" => $id_article,
            "nom_article" => $nom_article,
            "nom_utilisateur" => $nom_utilisateur,
            "vendeur" => $vendeur,
            "date_de_publication" => $date_de_publication,
            "date_vendu" => $date_vendu,
            "commentaire_acheteur" => $commentaire_acheteur
        );
 
       	if($nom_utilisateur!=null){
			array_push($articles_arr["records"], $article_item);
		}
    }
 
    // set response code - 200 OK
    http_response_code(200);
    // show products data in json format
    echo json_encode($articles_arr);
}
 
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
	var_dump($article->id_article);
    echo json_encode(
        array("message" => "No message found.")
    );
}