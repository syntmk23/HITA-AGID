<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate product object
include_once '../objects/chat.php';
 
$database = new Database();
$db = $database->getConnection();
 
$chat = new Chat($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// make sure data is not empty
if(
    !empty($data->description_chat) &&
    !empty($data->article_concerne) &&
    !empty($data->num_utilisateur) 
){
 
    // set product property values
    $chat->description_chat = $data->description_chat;
    $chat->article_concerne = $data->article_concerne;
    $chat->num_utilisateur = $data->num_utilisateur;
 
    // create the product
    if($chat->creer()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
		//var_dump($data->id_parleur);
		//var_dump($data->phrase);
		//var_dump($data->numero_chat);
		//var_dump($data->date_envoie);
        echo json_encode(array("message" => "chat was created."));
    }
 
    // if unable to create the product, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
		//var_dump($data->id_parleur);
		//var_dump($data->phrase);
		//var_dump($data->numero_chat);
		//var_dump($data->date_envoie);
        echo json_encode(array("message" => "Unable to create chat."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
	//var_dump($data->id_parleur);
	//var_dump($data->phrase);
	//var_dump($data->numero_chat);
	//var_dump($data->date_envoie);
    echo json_encode(array("message" => "Unable to create message. Data is incomplete."));
}
?>