<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/chat.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare chat object
$chat = new Chat($db);
 
// set ID property of record to read
$chat->num_utilisateur = isset($_GET['num_utilisateur']) ? $_GET['num_utilisateur'] : die();

//$chat->num_article = $data->num_article;

//var_dump($chat->num_utilisateur);
//var_dump($chat->num_article);

//y aura un bug si on decommente mais  il faut trouver moyen de recuperer le num article
//$chat->article_concerne = isset($_GET['num_article']) ? $_GET['num_article'] : die();

// read the details of chat to be edited
$chat->voir_un_seul();
 
if($chat->num_utilisateur!=null){
    // create array
    $chat_arr = array(
        "id_chat" =>  $chat->id_chat,
        "description_chat" => $chat->description_chat,
        "id_article" => $chat->id_article,
        "nom_article" => $chat->nom_article,
        "id_utilisateur" => $chat->id_utilisateur,
        "admin" => $chat->admin,
        "vendu" => $chat->vendu,
        "date_de_publication" => $chat->date_de_publication,
        "date_de_vente" => $chat->date_de_vente,
        "commentaire_acheteur" => $chat->commentaire_acheteur
    );
 
    // set response code - 200 OK
    http_response_code(200);
 
    // make it json format
    echo json_encode($chat_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user chat does not exist
    echo json_encode(array("message" => "the chat does not exist."));
}
?>