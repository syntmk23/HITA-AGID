<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/chat.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$chat = new Chat($db);
 // set ID property of record to read
$chat->num_utilisateur = isset($_GET['num_utilisateur']) ? $_GET['num_utilisateur'] : die();
// query products
$stmt = $chat->voir_chat_par_utilisateur();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $chats_arr=array();
    $chats_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $chat_item=array(
            "id_chat" => $id_chat,
            "description_chat" => $description_chat,
            "id_article" => $id_article,
            "nom_article" => $nom_article,
            "id_utilisateur" => $id_utilisateur,
            "admin" => $admin,
            "vendu" => $vendu,
            "date_de_publication" => $date_de_publication,
            "date_de_vente" => $date_de_vente,
            "commentaire_acheteur" => $commentaire_acheteur
        );
 
       	if($id_chat!=null){
			array_push($chats_arr["records"], $chat_item);
		}
    }
 
    // set response code - 200 OK
    http_response_code(200);
    // show products data in json format
    echo json_encode($chats_arr);
}
 
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No chat found.")
    );
}